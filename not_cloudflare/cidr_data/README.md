## CDN CIDR(IPv4) list  (cidr_cdnName.txt)

- Cloudflare CIDR is [cloudflare_inc/cloudflare_CIDR_v4.txt](../../cloudflare_inc/cloudflare_CIDR_v4.txt)




## ASN list  (asn_cdnName.txt)

- Cloudflare ASN is [cloudflare_inc/cloudflare_owned_ASN.txt](../../cloudflare_inc/cloudflare_owned_ASN.txt)



See [instructions.md](../../instructions.md) for file purpose and format specifications.