# Anti-Tor users FQDN list

- Not all Tor users are bad. Do not punish everyone.
- Blocking Tor is not a solution. There are VPNs, webproxies, and botnets.
- Also read: [Tor Project - Don't Block Me](https://trac.torproject.org/projects/tor/wiki/org/projects/DontBlockMe)


![](../../image/anonexist.jpg)



See [instructions.md](../../instructions.md) for file purpose and format specifications.