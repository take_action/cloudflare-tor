### License

* /globalist/* (Globalist) -- [GNU GPLv3](globalist/LICENSE)
* /pdf/* -- Unknown (you can find a copy everywhere. Thanks to authors)
* Else -- [PUBLIC DOMAIN (CC0)](https://web.archive.org/web/https://creativecommons.org/share-your-work/public-domain/cc0/)


This repository, _stop\_cloudflare (aka cloudflare-tor)_, is in the PUBLIC DOMAIN (CC0).

It was created anonymously, in public, for the use of the world to resist [Cloudflare](https://www.cloudflare.com/).  
  
Contributors who have anonymously contributed (including in [CryptoParty](https://cryptoparty.at/cryptoparty_wien_53)) have since come forward to give this project their blessing.  
