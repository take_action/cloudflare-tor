### Block Cloudflare MITM Attack

`Take action against Cloudflare`

![](../image/goodorbad.jpg)


```

This add-on will block, notify, or redirect your request if the target website is using Cloudflare.
Submit to global surveillance or resist. The choice is yours.
 
This add-on never send any data.
Your cloudflare-domain collection is yours.

```


Download add-on
- [Firefox ESR / Chromium](https://api.searxes.eu.org/_/addon.php?give&for=bcma)