### Will these links block Tor user?

`Scan FQDN using Searxes API`

![](../image/isatpreview.jpg)


```

You′ve found something on the internet.
Are these links blocking Tor users or not?
 
This add-on is using Searxes′ Public API to scan FQDN.
	e.g. https://ekzemplo.com/page.html → "ekzemplo.com"

This add-on never send other information.

```
 

Download add-on
- [Firefox ESR / Chromium](https://api.searxes.eu.org/_/addon.php?give&for=isattlink)