[Home](../README_short.md)

---

# CrimeFlarE
***Why you should say "No" to Cloudflare***


## Why Tor matters.
### Your users matter
Every visitor helps. Think about their experience. Cloudflare lets them wait.
Even users who don't use Tor get the "wait several seconds" messages. Users
often leave sites when they have to wait too long.

## Articles
- [7 thing you should know about Tor - EFF](https://www.eff.org/deeplinks/2014/07/7-things-you-should-know-about-tor)
- [Who uses Tor - Tor Project](https://www.torproject.org/about/torusers.html.en)
