### Are links vulnerable to MITM attack?

`Scan FQDN using Searxes API`

![](../image/ismmpreview.jpg)


```

You′ve found something on the internet.
Are these links or images vulnerable to MITM attack or not?
 
This add-on is using Searxes′ Public API to scan FQDN.
	e.g. https://ekzemplo.com/page.html → "ekzemplo.com"

This add-on never send other information.

```
 
- Information: [About Cloudflare domain list](../instructions.md#about-cloudflare-base-domain-list)


Download add-on
- [Firefox ESR / Chromium](https://api.searxes.eu.org/_/addon.php?give&for=ismitmlink)